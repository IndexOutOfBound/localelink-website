import React from "react";
import Sidebar from "../components/Sidebar";
import ReviewCard from "../components/CardLike";
import cardimg from "../assets/cardimg.png";
import person1 from "../assets/person1.png";
import ProfileReview from '../components/ProfileReview';

function Comment() {


  const cardData = [
    {
      title: 'Shrimp and Chorizo Paella',
      subheader: 'September 14, 2016',
      avatarImage: person1,
      cardMediaImage: cardimg,
      content: 'This impressive paella is a perfect party dish...',
      // Custom functions for each card
      onFavoriteClick: () => {
        console.log('Favorite clicked for card 1!');
      },
      onCommentClick: () => {
        console.log('Comment clicked for card 1!');
      },
    },
    
    // Add more card data objects as needed
  ];


  return (
    <div className="row"> 
      <div className="col-2 vh-100">
        <Sidebar />
      </div>
      <div className="col-10 mt-5">
        <div className="row justify-content-center">
          <div className="col-10 border-primary" style={{height:'100px'}}>
            <div className="row">
              <div className="col-12">
                {cardData.map((card, index) => (
                  <div key={index} style={{ marginBottom: '15px' }}>
                    <ReviewCard
                      key={index} // Use a unique key for each card
                      title={card.title}
                      subheader={card.subheader}
                      avatarImage={card.avatarImage}
                      cardMediaImage={card.cardMediaImage}
                      content={card.content}
                      onFavoriteClick={card.onFavoriteClick}
                      onCommentClick={card.onCommentClick}
                    />
                  </div>
                ))}
              </div>
              <div className="col-10">
                {cardData.map((card, index) => (
                    <div key={index} style={{ marginBottom: '15px' }}>
                        <ProfileReview
                        key={index} // Use a unique key for each card
                        title={card.title}
                        avatarImage={card.avatarImage}
                        reviews={card.reviews}
                        content={card.content}
                        onFavoriteClick={card.onFavoriteClick}
                        onCommentClick={card.onCommentClick}
                        />
                    </div>

                    ))}

              </div>
            </div>
          </div>
        </div>

       
      </div>
    </div>
  );
}

export default Comment;

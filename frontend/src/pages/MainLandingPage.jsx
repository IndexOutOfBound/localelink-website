import React from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';

const MainLandingPage = () => {
  return (
    <div className="landing-page">
      <Container>
        <Row className="justify-content-center">
          <Col md={8} className="text-center">
            <h1>Welcome to LocaleLink</h1>
            <p className="lead">Connecting communities around the world</p>
            <Button variant="primary" size="lg" className="mt-3">Get Started</Button>
          </Col>
        </Row>
        <Row className="mt-5">
          <Col md={4} className="text-center">
            <h2>Find Local Events</h2>
            <p>Discover and join events happening near you</p>
          </Col>
          <Col md={4} className="text-center">
            <h2>Connect with Locals</h2>
            <p>Meet people from your community and make new friends</p>
          </Col>
          <Col md={4} className="text-center">
            <h2>Explore Cultures</h2>
            <p>Experience different cultures through food, music, and more</p>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default MainLandingPage;

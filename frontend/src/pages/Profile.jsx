import React,{useEffect,useState} from "react";
import Sidebar from "../components/Sidebar";
import person1 from "../assets/person1.png";
import ProfileTabs from "../components/ProfileTab";
import { FaPhoneAlt } from 'react-icons/fa';
import axios from 'axios';
import moment from 'moment';
import { useNavigate } from "react-router-dom";

const API_BASE_URL = "http://localhost:3002"; 
function Profile() {
  const navigate = useNavigate();
  const getUserData=sessionStorage.getItem('userdata');
  let name='';
  let email='';
  let business_id=0;
  let userdata=null;
  if (!getUserData) {
    navigate('/')
  } else {
    userdata = JSON.parse(getUserData);
      email = userdata.email;
      name = userdata.name;
      business_id = userdata.business_id
  }
  
        const [data, setData] = useState([]);
        useEffect(() => {
          fetchData();
        }, []);

        const fetchData = async () => {
          try {
            const id = business_id;
            const response = await axios.get(`${API_BASE_URL}/business/${id}`);
            setData(response.data);
            console.log(response.data);
          } catch (e) {
            console.log("Failed to fetch data");
            toast.error("Unable to fetch data");
          }
        };
        console.log(data)
  return (
    <div className="container-fluid">
      <div className="row">
        {/* Sidebar */}
        <div className="col-md-2 vh-100">
          <Sidebar />
        </div>

        {/* Profile */}
        <div className="col-md-10">
          <div className="row justify-content-center mt-5">
            {/* Profile Image */}
            <div className="col-md-2">
              <img src={person1} alt="Profile" className="img-fluid rounded-circle" style={{ width: '150px', height: '150px' }} />
            </div>

            {/* Profile Details */}
            <div className="col-md-6">
              <h3>{data.name}</h3>
              <p className="mb-1">{data.location}</p>
              <p className="mb-1">{data.category}</p>
              <p className="mb-1">{data.email}</p>
              <p className="mb-1"><FaPhoneAlt /> +975-{data.phone}</p>
              <p className="mb-1">{data.description}</p>
            </div>
          </div>

          {/* Profile Tabs */}
          <div className="row justify-content-center mt-5">
            <div className="col-md-8">
              <ProfileTabs />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Profile;

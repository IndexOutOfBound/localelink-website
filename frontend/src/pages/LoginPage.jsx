import React from 'react';
import { useState } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import { Form, Button, Col, Alert } from 'react-bootstrap';
import axios from 'axios';
import logo from '../assets/Logo.svg';

const API_BASE_URL = "http://localhost:3002";

function LoginPage() {
  const navigate = useNavigate();
  const [formData, setFormData] = useState({
    email:'',
    password:'',
  });

  const [alertMessage, setAlertMessage] = useState(null);

  const showAlert = (message, variant) => {
    setAlertMessage({ message, variant });
    setTimeout(() => {
      setAlertMessage(null);
    }, 3000);
  };

  const handleChange = (event) => {
    setFormData({ ...formData, [event.target.name]: event.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post(`${API_BASE_URL}/business/login`, formData);
      if (response.status === 200) {
        const userdata = {
          business_id: response.data.business_id,
          name: response.data.name,
          email: response.data.email,
        };
        console.log(userdata);
        
        sessionStorage.setItem('userdata', JSON.stringify(userdata));

        navigate('/homepage');
      }
    } catch (e) {
      console.log(e);
      showAlert(e.response.data.message, "danger");
    }
  };

  return (
    <div className="d-flex justify-content-center align-items-center min-vh-100 bg-light position-relative">
      {/* Logo positioned at the top left corner */}
      <div className="position-absolute" style={{ top: '20px', left: '20px' }}>
        <img src={logo} alt="Logo" style={{ width: '100px', height: '100px' }} />
      </div>

      <Col xs={12} md={6} lg={4}>
        <div className="card shadow border-0" style={{ marginTop: '100px' }}>
          <div className="card-body">
            {alertMessage && (
              <Alert variant={alertMessage.variant} className="position-fixed bottom-0 end-0 m-3">
                {alertMessage.message}
              </Alert>
            )}
            <h3 className="card-title text-center mb-4">Welcome</h3>
            <Form onSubmit={handleSubmit}>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" autoComplete="email" name="email" value={formData.email} onChange={handleChange} required />
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" autoComplete="current-password" name='password' value={formData.password} onChange={handleChange} required />
                <Form.Text className="text-muted">
                  <a href="#" className="text-muted">Forgot password?</a>
                </Form.Text>
              </Form.Group>
              <Button variant="primary" type="submit" className="w-100">
                Sign in
              </Button>
            </Form>
            <Link to="/register" >Dont have an account?</Link>
          </div>
        </div>
      </Col>
    </div>
  );
}

export default LoginPage;

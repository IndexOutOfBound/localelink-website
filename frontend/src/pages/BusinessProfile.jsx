import React, { useEffect, useState } from "react";
import SidebarUser from "../components/SidebarUser";
import person1 from "../assets/person1.png";
import BusinessProfiletab from "../components/BusinessProfiletab";
import { FaPhoneAlt } from 'react-icons/fa';
import axios from 'axios';
import { useNavigate, useParams } from "react-router-dom";
import { toast } from 'react-toastify';

const API_BASE_URL = "http://localhost:3002"; 

function BusinessProfile() {
  const business_id = 1; // Ensure this is correctly set or retrieved
  const [data, setData] = useState({});

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get(`${API_BASE_URL}/business/${business_id}`);
      setData(response.data);
    } catch (e) {
      console.log("Failed to fetch data");
      toast.error("Unable to fetch data");
    }
  };

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-md-2 vh-100">
          <SidebarUser />
        </div>

        <div className="col-md-10">
          <div className="row justify-content-center mt-5">
            <div className="col-md-2">
              <img src={person1} alt="Profile" className="img-fluid rounded-circle" style={{ width: '150px', height: '150px' }} />
            </div>

            <div className="col-md-6">
              <h3>{data.name}</h3>
              <p className="mb-1">{data.location}</p>
              <p className="mb-1">{data.category}</p>
              <p className="mb-1">{data.email}</p>
              <p className="mb-1"><FaPhoneAlt /> +975-{data.phone}</p>
              <p className="mb-1">{data.description}</p>
            </div>
          </div>

          <div className="row justify-content-center mt-5">
            <div className="col-md-8">
              <BusinessProfiletab />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default BusinessProfile;

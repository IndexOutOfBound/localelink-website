import React from 'react'
import Message from '../components/Message'
import Sidebar from '../components/Sidebar';
function Notification() {
    const messages = [
        {
          profilePic: 'https://via.placeholder.com/40',
          messageText: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor.',
        },
        {
          profilePic: 'https://via.placeholder.com/40',
          messageText: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor.',
        },
        // Add more message objects as needed
      ];
    return (
    <div className="row"> 
      <div className="col-2 vh-100">
        <Sidebar />
      </div>
      <div className="col-10">
      <div className="row justify-content-center">
          <div className="col-10 mb-5 mt-3">
            {messages.map((message, index) => (
                <Message key={index} profilePic={message.profilePic} messageText={message.messageText} />
            ))}
         </div>
      </div>
    </div>
    </div>
  )
}

export default Notification
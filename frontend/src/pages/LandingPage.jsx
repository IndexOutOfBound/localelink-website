import React, { useEffect, useState } from "react";
import Sidebar from "../components/Sidebar";
import ReviewCard from "../components/CardLike";
import ImageCarousel from '../components/Carousel';
import person1 from "../assets/person1.png";
import { toast } from 'react-toastify';
import axios from 'axios';
import moment from 'moment';
import { useNavigate } from "react-router-dom";

const API_BASE_URL = "http://localhost:3002"; // Ensure this matches your server setup

function LandingPage() {
  const navigate = useNavigate();
  const getUserData=sessionStorage.getItem('userdata');
  let name='';
  let email='';
  let business_id=0;
  let userdata=null;
  if (!getUserData) {
    navigate('/')
  } else {
    userdata = JSON.parse(getUserData);
      email = userdata.email;
      name = userdata.name;
      business_id = userdata.business_id
  }
 

  const [data, setData] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get(`${API_BASE_URL}/posts`);
      setData(response.data.data);
      console.log(response.data.data);
    } catch (e) {
      console.log("Failed to fetch data");
      toast.error("Unable to fetch data");
    }
  };

  const imageArray = [person1, person1, person1, person1, person1, person1, person1];

  return (
    <div className="row">
      <div className="col-2 vh-100">
        <Sidebar />
      </div>
      <div className="col-10">
        <div className="row justify-content-center">
          <div className="col-11 mb-5 mt-3" style={{ height: '120px' }}>
            <ImageCarousel imageArray={imageArray} />
          </div>
          <div className="col-10 border-primary" style={{ height: '100px' }}>
            <div className="row">
              <div className="col-12">
                {data.map((post) => (
                  <div key={post.post_id} style={{ marginBottom: '15px' }}>
                    <ReviewCard
                      user_id={business_id}
                      post_id={post.post_id}
                      title={post.business ? post.business.name : 'Unknown Business'}
                      subheader={moment(post.createdAt).format('MMMM Do YYYY, h:mm:ss a')}
                      avatarImage={person1} // Replace with actual image if available
                      cardMediaImage={post.image_path}
                      content={post.image_captions || 'No caption provided'}
                    />
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LandingPage;

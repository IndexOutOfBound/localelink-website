import React, { useState } from 'react';
import SidebarUser from '../components/SidebarUser';
import tiger from '../assets/tiger.jpg';
import wolf from '../assets/wolf.jpg';
import panda from '../assets/panda.jpg';
import alcohol from '../assets/alcohol.jpg';
import res from '../assets/res.jpg';
import res1 from '../assets/res1.jpg'; // Assuming you have res1.jpg
import res2 from '../assets/res2.jpg'; // Assuming you have res2.jpg
import Food from '../assets/Food.jpg';
import date from '../assets/date.jpg';
import books from '../assets/books.jpg';
import gro from '../assets/gro.jpg';
import cloth1 from '../assets/cloth1.jpg';
import cloth2 from '../assets/cloth2.jpg';
import cloth3 from '../assets/cloth3.jpg';
import person1 from '../assets/person1.png';

const UserFavourites = () => {
  const categories = ['Restaurant', 'Clothes', 'Electronics', 'Books', 'Groceries', 'Food', 'Date', 'Alcohol', 'Pets', 'Pillow'];
  const shops = {
    Restaurant: ['Restaurant 1', 'Restaurant 2', 'Restaurant 3'],
    Clothes: ['Clothes 1', 'Clothes 2', 'Clothes 3'],
    Electronics: ['Electronics 1', 'Electronics 2', 'Electronics 3'],
    Books: ['Book 1', 'Book 2', 'Book 3'],
    Groceries: ['Groceries 1', 'Groceries 2', 'Groceries 3'],
    Food: ['Food 1', 'Food 2', 'Food 3'],
    Date: ['Date 1', 'Date 2', 'Date 3'],
    Alcohol: ['Alcohol 1', 'Alcohol 2', 'Alcohol 3'],
    Pets: ['Pets 1', 'Pets 2', 'Pets 3'],
    Pillow: ['Pillow 1', 'Pillow 2', 'Pillow 3'],
  };

  const images = {
    Restaurant: {
      'Restaurant 1': [res, res1,panda,wolf,panda,wolf],
      'Restaurant 2': [res2, res],
      'Restaurant 3': [res1, res2],
    },
    Clothes: {
      'Clothes 1': [cloth1,cloth2,cloth3],
      'Clothes 2': [cloth1],
      'Clothes 3': [cloth2],
    },
    Electronics: {
      'Electronics 1': [panda],
      'Electronics 2': [panda],
      'Electronics 3': [panda],
    },
    Books: {
      'Book 1': [books],
      'Book 2': [books],
      'Book 3': [books],
    },
    Groceries: {
      'Groceries 1': [gro],
      'Groceries 2': [gro],
      'Groceries 3': [gro],
    },
    Food: {
      'Food 1': [Food],
      'Food 2': [Food],
      'Food 3': [Food],
    },
    Date: {
      'Date 1': [date],
      'Date 2': [date],
      'Date 3': [date],
    },
    Alcohol: {
      'Alcohol 1': [alcohol],
      'Alcohol 2': [alcohol],
      'Alcohol 3': [alcohol],
    },
    Pets: {
      'Pets 1': [panda,wolf,person1],tiger,
      'Pets 2': [person1],
      'Pets 3': [person1],
    },
    Pillow: {
      'Pillow 1': [panda],
      'Pillow 2': [panda],
      'Pillow 3': [panda],
    },
  };

  // State variables to keep track of the selected category and images
  const [selectedCategory, setSelectedCategory] = useState(categories[0]);
  const [selectedImages, setSelectedImages] = useState(images['Restaurant']['Restaurant 1']);

  // Function to handle the selection of shop
  const handleShopSelection = (category, shop) => {
    setSelectedCategory(category);
    setSelectedImages(images[category][shop]);
  };

  return (
    <div style={styles.container}>
      <div style={styles.sidebar}>
        <SidebarUser />
      </div>
      <div style={styles.mainContent}>
        <div style={styles.userFavourites}>
          <div style={styles.containerbar}>
            <div style={styles.horizontalScrollBar}>
              {categories.map((category) => (
                <div style={styles.scrollItem} key={category} onClick={() => setSelectedCategory(category)}>
                  {category}
                </div>
              ))}
            </div>
          </div>
          <div style={styles.content}>
            <div style={styles.shopList}>
              {shops[selectedCategory].map((shop) => (
                <div
                  style={styles.shopItem}
                  key={shop}
                  onClick={() => handleShopSelection(selectedCategory, shop)}
                >
                  {shop}
                </div>
              ))}
            </div>
            <div style={styles.posts}>
              {selectedImages.map((image, index) => (
                <div style={styles.postItem} key={index}>
                  <img style={styles.image} src={image} alt={`Selected Post ${index}`} />
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const styles = {
  container: {
    display: 'flex',
    height: '100vh',
  },
  containerbar: {
    display: 'flex',
    height: '20vh',
    borderBottom: '1px solid orange',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  image: {
    width: '400px',
    height: '200px',
  },
  sidebar: {
    width: '250px',
    backgroundColor: '#f8f9fa',
    width: '20vw',
  },
  mainContent: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    width: '80vw',
   
  },
  userFavourites: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
   
    
 
  },
  horizontalScrollBar: {
    display: 'flex',
    overflowX: 'auto',
    whiteSpace: 'nowrap',
    color: 'white',
    padding: '20px',
    width: '100%',
    
  },
  scrollItem: {
    flex: '0 0 auto',
    marginRight: '15px',
    padding: '10px 20px',
    backgroundColor: 'green',
    borderRadius: '5px',
    cursor: 'pointer',
    width: '10%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    flex: 2,
    display: 'flex',
   
  },
  shopList: {
    flex: 1,
    overflowY: 'auto',
    backgroundColor: '#f8f9fa',
    padding: '10px',
  },
  shopItem: {
    padding: '10px',
    borderBottom: '1px solid #dee2e6',
    cursor: 'pointer',
  },
  posts: {
    flex: 2,
    backgroundColor: '#ffffff',
    borderLeft: '1px solid #dee2e6',
   padding: '10px',
    display: 'flex',
    flexWrap: 'wrap',
    overflow:'auto'
  },
  postItem: {
    padding: '5px',
    paddingbottom:0,
    flex: '1 0 10%', // Adjust this to control the size and wrapping of images
    boxSizing: 'border-box',
  },
};

export default UserFavourites;

import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Container, Row, Col, Form, Button, Image , Alert} from 'react-bootstrap';
import Sidebar from '../components/Sidebar';
import axios from 'axios';
import {toast} from 'react-toastify'
const API_BASE_URL = 'http://localhost:3002'

function CreatePost() {

  


  const navigate = useNavigate();
  const getUserData = sessionStorage.getItem('userdata');
  let business_id = 0;
  let name = '';
  let email = '';
  let userdata = null;
  if (!getUserData) {
    navigate('/');
  } else {
    userdata = JSON.parse(getUserData);
    business_id = userdata.business_id;
    email = userdata.email;
    name = userdata.name;
  }
  const [selectedFile, setSelectedFile] = useState(null);
  const [previewUrl, setPreviewUrl] = useState(null);
  const [caption, setCaption] = useState('');


  const handleFileChange = (event) => {
    const file = event.target.files[0];
    setSelectedFile(file);

    // Create a preview URL for the uploaded image
    const reader = new FileReader();
    reader.onloadend = () => {
      setPreviewUrl(reader.result);
    };
    reader.readAsDataURL(file);
  };

  const handleCaptionChange = (event) => {
    setCaption(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const formData = new FormData();
      formData.append('image', selectedFile);
      formData.append('caption', caption);
      formData.append('business_id', business_id); // Assuming you need to send business_id to the backend

      // Make a POST request to your backend API endpoint
      const response = await axios.post(`${API_BASE_URL}/posts`, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });

      console.log(response.data.message); 
      toast.success(response.data.message)
    } catch (error) {
      console.error('Error creating post:', error);
      toast.error(error.response.data.message)
    }
  };

  return (
    <div className="row">
      <div className="col-2 vh-100">
        <Sidebar />
      </div>
      <div className="col-10">
        <div className="row justify-content-center">
          <Col md={8}>
            <Form onSubmit={handleSubmit}>
              <Form.Group controlId="formFileUpload">
                <Form.Label>Upload Photo</Form.Label>
                <Form.Control type="file" name='file' onChange={handleFileChange} />
              </Form.Group>
              {previewUrl && <Image src={previewUrl} fluid className="mt-3" />}
              <Form.Group controlId="formCaption" className="mt-3">
                <Form.Label>Caption</Form.Label>
                <Form.Control
                  type="text"
                  name='captions'
                  value={caption}
                  onChange={handleCaptionChange}
                  placeholder="Enter a caption for your photo"
                />
              </Form.Group>
              <Button variant="primary" type="submit" className="mt-3">
                POST
              </Button>
            </Form>
          </Col>
        </div>
      </div>
    </div>
  );
}

export default CreatePost;

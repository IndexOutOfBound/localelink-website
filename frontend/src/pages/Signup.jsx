import React, { useState } from "react";
import axios from "axios";
import { Container, Row, Col, Form, Button, Alert } from "react-bootstrap";
import { useNavigate, Link } from "react-router-dom";
import logo from '../assets/Logo.svg';

const API_BASE_URL = "http://localhost:3002";

function Signup() {
  const navigate = useNavigate();
  const [alertMessage, setAlertMessage] = useState(null);

  const showAlert = (message, variant) => {
    setAlertMessage({ message, variant });
    if (variant === "success") {
      setTimeout(() => {
        setAlertMessage(null);
        navigate("/userlogin");
      }, 3000);
    } else {
      setTimeout(() => {
        setAlertMessage(null);
      }, 3000);
    }
  };

  const [formData, setFormData] = useState({
    name: "",
    email: "",
    phone: "",
    password: "",
  });

  const handleChange = (event) => {
    setFormData({ ...formData, [event.target.name]: event.target.value });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const response = await axios.post(`${API_BASE_URL}/user/register`, formData);
      showAlert(response.data.message, "success");
    } catch (err) {
      showAlert(err.response.data.message, "danger");
    }
  };

  return (
    <Container className="my-5">
      <div className="position-absolute" style={{ top: '20px', left: '20px' }}>
        <img src={logo} alt="Logo" style={{ width: '100px', height: '100px' }} />
      </div>
      <Row className="justify-content-center">
        <Col md={6}>
          <h3 className="text-center mb-4">New Here?</h3>
          <h6 className="text-center mb-4">Join us today!</h6>
          {alertMessage && (
            <Alert variant={alertMessage.variant} className="position-fixed bottom-0 end-0 m-3">
              {alertMessage.message}
            </Alert>
          )}
          <Form onSubmit={handleSubmit}>
            <Form.Group controlId="formName" className="mb-3">
              <Form.Label>Full Name</Form.Label>
              <Form.Control
                type="text"
                name="name"
                value={formData.name}
                onChange={handleChange}
                required
              />
            </Form.Group>

            <Form.Group controlId="formEmail" className="mb-3">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                name="email"
                value={formData.email}
                onChange={handleChange}
                required
              />
            </Form.Group>

            <Form.Group controlId="formPhone" className="mb-3">
              <Form.Label>Phone Number</Form.Label>
              <Form.Control
                type="text"
                name="phone"
                value={formData.phone}
                onChange={handleChange}
                required
              />
            </Form.Group>

            <Form.Group controlId="formPassword" className="mb-3">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                name="password"
                value={formData.password}
                onChange={handleChange}
                required
              />
            </Form.Group>

            <Button variant="primary" type="submit" className="w-100 mt-3">
              Sign Up
            </Button>
          </Form>
          <div className="text-center mt-3">
            <Link to="/">Go back to login page</Link>
          </div>
        </Col>
      </Row>
    </Container>
  );
}

export default Signup;

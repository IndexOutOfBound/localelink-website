
import LandingPage from './pages/LandingPage'
import LoginPage from './pages/LoginPage'
import {Routes, Route, BrowserRouter} from 'react-router-dom';
import RegisterPage from './pages/RegisterPage';
import Sidebar from './components/Sidebar';
import Profile from './pages/Profile';
import Comment from './pages/Comment';
import CreatePost from './pages/CreatePost';
import Notification from './pages/Notification';
import { ToastContainer } from 'react-toastify';
import MainLandingPage from './pages/MainLandingPage';
import 'react-toastify/dist/ReactToastify.css'
import Signup from './pages/Signup';
import Signin from './pages/Signin';
import UserHomePage from './pages/UserHomePage';
import UserNotification from './pages/UserNotification';
import BusinessProfile from './pages/BusinessProfile';
import UserFavourites from './pages/UserFavourites';
function App() {
  return (
        <BrowserRouter>
           <ToastContainer
            position="bottom-left"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            theme="colored"
            transition: Bounce
            />
          <Routes>
            <Route path='/' element={<LoginPage/>}/>
            <Route path='/mlandingpage' element={<MainLandingPage/>}/>
            <Route path='/homepage' element={<LandingPage/>} />
            <Route path='/usernotifications' element={<UserNotification/>}/>
            <Route path='/userhomepage' element={<UserHomePage/>}/>
            <Route path='/register' element={<RegisterPage/>} />
            <Route path='/userregister' element={<Signup/>} />
            <Route path='/userlogin' element={<Signin/>} />
            <Route path='/sidebar' element={<Sidebar/>}/>
            <Route path='/profile' element={<Profile />}/>
            <Route path='/comment' element={<Comment />}/>
            <Route path='/createpost' element={<CreatePost />}/>
            <Route path='/notifications' element={<Notification/>}/>
            <Route path='/businessprofile/:business_id' element={<BusinessProfile />}/>
            <Route path='/userfavourites' element={<UserFavourites/>}/>
        </Routes>
        </BrowserRouter>
        

)
   
}

export default App

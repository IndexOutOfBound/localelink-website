import React, { useState, useEffect } from 'react';
import { Table, Button, Form } from 'react-bootstrap';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

const API_BASE_URL = 'http://localhost:3002';

const ProfileForm = () => {
  const navigate = useNavigate();
  const getUserData = sessionStorage.getItem('userdata');
  let name = '';
  let email = '';
  let business_id = 0;
  let userdata = null;
  if (!getUserData) {
    navigate('/');
  } else {
    userdata = JSON.parse(getUserData);
    email = userdata.email;
    name = userdata.name;
    business_id = userdata.business_id;
  }

  const [services, setServices] = useState([]);
  const [newService, setNewService] = useState({ service: '', price: '' });

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get(`${API_BASE_URL}/service/business/${business_id}`);
      setServices(response.data);
    } catch (error) {
      console.error('Error fetching services:', error);
    }
  };

  // const handleInputChange = (e) => {
  //   const { name, value } = e.target;
  //   setNewService({ ...newService, [name]: value });
  // };
  const [service, setService]= useState('')
  const [price,setPrice]= useState('')

  const handleAddService = async () => {
    // Check if both service name and price are provided
    if (!service || !price) {
      toast.error('Please enter both service name and price');
      return;
    }
    
    try {
      await axios.post(`${API_BASE_URL}/service`, { service,price, business_id });
      toast.success('Service added successfully');
      fetchData();
      setService('')
      setPrice('')
    } catch (error) {
      console.error('Error adding service:', error);
      toast.error('Error adding service');
    }
  };

  const handleDeleteService = async (id) => {
    try {
      await axios.delete(`${API_BASE_URL}/service/${id}`);
      toast.success('Service deleted successfully');
      fetchData();
    } catch (error) {
      console.error('Error deleting service:', error);
    }
  };

  return (
    <div>
      <h3>Services</h3>
      <Form>
        <Form.Group className="mb-3">
          <Form.Label>Service Name</Form.Label>
          <Form.Control type="text" placeholder="Enter service name" name="service" value={service} onChange={(e)=>setService(e.target.value)} />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Price</Form.Label>
          <Form.Control type="text" placeholder="Enter price" name="price" value={price} onChange={(e)=>setPrice(e.target.value)} />
        </Form.Group>
        <Button variant="primary" onClick={handleAddService}>
          Add Service
        </Button>
      </Form>
      <Table striped bordered hover className="mt-3">
        <thead>
          <tr>
            <th>#</th>
            <th>Service Name</th>
            <th>Price</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {services.map((service, index) => (
            <tr key={service.id}>
              <td>{index + 1}</td>
              <td>{service.name}</td>
              <td>{service.price}</td>
              <td>
                <Button variant="danger" onClick={() => handleDeleteService(service.id)}>
                  Delete
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default ProfileForm;

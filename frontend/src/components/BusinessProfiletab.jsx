import React, { useEffect, useState } from 'react';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import { toast } from 'react-toastify';
import axios from 'axios';

import BusinessProfileForm from './BusinessProfileForm';
import Gallery from './Gallery';
import BusinessProfileReview from './BusinessProfileReview';

const API_BASE_URL = "http://localhost:3002";

export default function ColorTabs() {
  const business_id = 1;
  const [data, setData] = useState([]);
  const [value, setValue] = useState(0);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get(`${API_BASE_URL}/posts/business/${business_id}`);
      setData(response.data);
      console.log(response.data);
    } catch (e) {
      console.log("Failed to fetch data");
      toast.error("Unable to fetch data");
    }
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box sx={{ width: '100%' }}>
      <Tabs
        value={value}
        onChange={handleChange}
        textColor="secondary"
        indicatorColor="secondary"
        aria-label="secondary tabs example"
      >
        <Tab label="Menu/ Service" />
        <Tab label="Posts" />
        <Tab label="Reviews" />
      </Tabs>
      <TabPanel value={value} index={0}>
        <BusinessProfileForm />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Gallery itemData={data} />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <BusinessProfileReview />
      </TabPanel>
    </Box>
  );
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Box
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          {children}
        </Box>
      )}
    </Box>
  );
}

import React from 'react';
import person1 from '../assets/person1.png'

const Message = ({ profilePic, messageText }) => {
  return (
    <div className="d-flex align-items-start mb-3">
      <img src={person1} alt="Profile" className="rounded-circle me-3 mt-4" style={{ width: '40px', height: '40px' }} />
      <div className="bg-light rounded p-3">
        {messageText}
      </div>
    </div>
  );
};

export default Message;

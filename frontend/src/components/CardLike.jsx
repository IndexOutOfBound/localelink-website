import React, { useState, useEffect } from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import FavoriteIcon from '@mui/icons-material/Favorite';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import CommentIcon from '@mui/icons-material/Comment';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import axios from 'axios';
import moment from 'moment';
import { comment } from 'postcss';
import {Link} from 'react-router-dom'

const API_BASE_URL = "http://localhost:3002";

export default function ReviewCard({ user_id, post_id, title, subheader, content, avatarImage, cardMediaImage }) {
  const [isExpanded, setIsExpanded] = useState(false);
  const [isLiked, setIsLiked] = useState(false);
  const [comments, setComments] = useState([]);
  const [newComment, setNewComment] = useState('');
  const contentLimit = 100; // Set your content length limit

  const handleReadMore = () => {
    setIsExpanded(!isExpanded);
  };

  const fetchComments = async () => {
    try {
      const response = await axios.get(`${API_BASE_URL}/comments/${post_id}`);
      setComments(response.data.comments);
    } catch (e) {
      console.log('Failed to fetch comments');
    }
  };

  useEffect(() => {
    fetchComments();
  }, []);
  
  const handleLike = async () => {
    try {
      if (isLiked) {
        // Unlike the post
        setIsLiked(false);
        // Implement your logic to remove the like from the backend
        await axios.delete(`${API_BASE_URL}/likes`,{user_id, post_id})
      } else {
        // Like the post
        setIsLiked(true);
        // Implement your logic to add the like to the backend
        await axios.post(`${API_BASE_URL}/likes`, { user_id, post_id });
        console.log('Post liked');
      }
    } catch (e) {
      console.log('Failed to like post');
    }
  };

  const handleComment = async () => {
    try {
      await axios.post(`${API_BASE_URL}/comments`, { user_id, post_id, content: newComment });
      setNewComment('');
      fetchComments(); // Refresh comments after adding
    
    } catch (e) {
      console.log('Failed to add comment');
    }
  };

  return (
    <Card sx={{ maxWidth: '100%' }}>
      <Link to={`/businessprofile/${user_id}`}>
        <CardHeader
          avatar={<Avatar alt="Avatar" src={avatarImage} />}
          action={<IconButton aria-label="settings"><MoreVertIcon /></IconButton>}
          title={title}
          subheader={subheader}
        />
      </Link>
      <CardMedia
        style={{ height: '450px', objectFit: 'cover' }}
        component="img"
        src={`${API_BASE_URL}/${cardMediaImage}`}
        alt="Image"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          {isExpanded ? content : `${content.slice(0, contentLimit)}...`}
          {content.length > contentLimit && (
            <Typography 
              variant="body2" 
              color="primary" 
              component="span" 
              onClick={handleReadMore}
              style={{ cursor: 'pointer' }}
            >
              {isExpanded ? ' Show Less' : ' Read More'}
            </Typography>
          )}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites" onClick={handleLike}>
          {isLiked ? <FavoriteIcon /> : <FavoriteBorderIcon />}
        </IconButton>
        <IconButton aria-label="comment" onClick={() => setIsExpanded(!isExpanded)}>
          <CommentIcon />
        </IconButton>
      </CardActions>
      {isExpanded && (
        <CardContent>
          <Typography variant="h6">Comments</Typography>
          {comments.map(comment => (
            <Typography key={comment.comment_id} variant="body2" color="textSecondary" component="p">
              {comment.content}
            </Typography>
          ))}
          <TextField
            label="Add a comment"
            variant="outlined"
            fullWidth
            value={newComment}
            onChange={(e) => setNewComment(e.target.value)}
            style={{ marginTop: '10px', }}
          />
          <Button
            variant="contained"
            color="primary"
            onClick={handleComment}
            style={{ marginTop: '10px' }}
          >
            Submit
          </Button>
        </CardContent>
      )}
    </Card>
  );
}

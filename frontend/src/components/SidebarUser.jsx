import React from 'react';
import { HomeIcon, PlusIcon, BellIcon, UserIcon, } from '@heroicons/react/outline'; // Ensure you have @heroicons/react package installed
import logo from '../assets/Logo.svg';
import background from '../assets/background.png';

const navigation = [
  { link: "/userhomepage", name: 'Home', icon: <HomeIcon style={{height:40, width:40}}  /> },
  { link: "/userfavourites", name: 'Favourites', icon: <UserIcon  style={{height:40, width:40}} /> },
  { link: "/usernotifications", name: 'Notifications', icon: <BellIcon  style={{height:40, width:40}} /> },
  {link: "/userlogin", name: 'Logout', icon : <HomeIcon style={{height:40, width:40}} /> },
];

export default function SidebarUser() {
  return (
    <div className="bg-dark text-white vh-100 d-flex flex-column " style={{ position: 'fixed', top: 0, left: 0, width: '20vw', zIndex: 1000, backgroundImage: `url(${background})`, backgroundPosition: 'contain',alignItems:'center', }}>
      {/* Logo Section */}
      <div className="d-flex align-items-center justify-content-center  pt-5">
        <img src={logo} alt="Logo" className="p-1 img-fluid ms-1" />
      </div>
      {/* Navigation Menu */}
      <ul className="nav flex-column mt-3 p-4">
        {/* Dynamic menu items */}
        {navigation.map((menuItem, index) => (
          <li className="nav-item" key={index}>
            <a href={menuItem.link} className="nav-link text-white p-3 hover-bg-primary d-flex align-items-center">
              {menuItem.icon} {/* Container for icon */}
              <span className="d-none d-md-inline ms-2">{menuItem.name}</span> {/* Added ms-2 class for margin to the left of the label */}
            </a>
          </li>
        ))}
        {/* Logout (Static) */}
        {/* <li className="nav-item mt-5 pt-5">
          <button onClick={handleLogout} className='bg-dark border-0 p-0 text-decoration-none text-white'>
          <Link to={''} className="nav-link text-white p-3 hover-bg-primary mt-5 mb-5 d-flex align-items-center">
            <RiLogoutBoxLine className="me-2 d-md-block" />
            <span className="d-none d-md-inline">Logout</span>
          </Link>
          </button>
        </li> */}
      </ul>
    </div>
  );
}

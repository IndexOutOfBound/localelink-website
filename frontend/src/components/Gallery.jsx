import * as React from 'react';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
const API_BASE_URL = "http://localhost:3002";
export default function Gallery({ itemData }) {
  return (
    <ImageList sx={{ width: "100%", height: 450 }} cols={3} rowHeight={164}>
      {itemData.map((item) => (
        <ImageListItem key={item.post_id}> {/* Use a unique key for each image */}
          <img
            src={`${API_BASE_URL}/${item.image_path}`} 
            alt={item.image_captions} 
            loading="lazy"
          />
        </ImageListItem>
      ))}
    </ImageList>
  );
}
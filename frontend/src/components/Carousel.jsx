import React, { useState } from 'react';
import { Carousel } from 'react-bootstrap';
import { BsChevronLeft, BsChevronRight } from 'react-icons/bs'; // Import icons from react-icons

const ImageCarousel = ({ imageArray }) => {
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };

  return (
    <div className="w-100">
      <Carousel activeIndex={index} onSelect={handleSelect} indicators={false} nextIcon={<BsChevronRight className="text-black" />} prevIcon={<BsChevronLeft className="text-black" />}>
        {[...Array(Math.ceil(imageArray.length / 5))].map((_, slideIndex) => (
          <Carousel.Item key={slideIndex}>
            <div className="d-flex justify-content-around">
              {imageArray.slice(slideIndex * 5, (slideIndex + 1) * 5).map((image, idx) => (
                <img
                  key={slideIndex * 5 + idx}
                  src={image}
                  alt={`Image ${slideIndex * 5 + idx}`}
                  className="rounded-circle img-fluid"
                  style={{ maxHeight: '200px', maxWidth: '200px' }}
                />
              ))}
            </div>
          </Carousel.Item>
        ))}
      </Carousel>
      
    </div>
  );
};

export default ImageCarousel;

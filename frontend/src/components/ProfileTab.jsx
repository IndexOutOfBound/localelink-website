import React,{useEffect,useState} from 'react';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Gallery from './Gallery';
import ProfileReview from './ProfileReview';
import person1 from "../assets/person1.png";
import ProfileForm from './ProfileForm';

import axios from 'axios';
import moment from 'moment';
import { useNavigate } from "react-router-dom";

const API_BASE_URL = "http://localhost:3002"; 

export default function ColorTabs() {

  const navigate = useNavigate();
  const getUserData=sessionStorage.getItem('userdata');
  let name='';
  let email='';
  let business_id=0;
  let userdata=null;
  if (!getUserData) {
    navigate('/')
  } else {
    userdata = JSON.parse(getUserData);
      email = userdata.email;
      name = userdata.name;
      business_id = userdata.business_id
  }

  const [data, setData] = useState([]);
  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get(`${API_BASE_URL}/posts/business/${business_id}`);
      setData(response.data);
      console.log(response.data);
    } catch (e) {
      console.log("Failed to fetch data");
      toast.error("Unable to fetch data");
    }
  };
  console.log(data);

  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const cardData = [
    {
      title: 'Shrimp and Chorizo Paella',
      subheader: 'September 14, 2016',
      avatarImage: person1,
      reviews:" This impressive paella is a perfect party dish.",
      
      // Custom functions for each card
      onFavoriteClick: () => {
        console.log('Favorite clicked for card 1!');
      },
      onCommentClick: () => {
        console.log('Comment clicked for card 1!');
      },
    },
    {
      title: 'Another Dish',
      subheader: 'October 20, 2016',
      avatarImage: person1,
      reviews:" This impressive paella is a perfect party dish.",
     
      // Custom functions for each card
      onFavoriteClick: () => {
        console.log('Favorite clicked for card 2!');
      },
      onCommentClick: () => {
        console.log('Comment clicked for card 2!');
      },
    },
    // Add more card data objects as needed
  ];

  return (
    <Box sx={{ width: '100%' }}>
      <Tabs
        value={value}
        onChange={handleChange}
        textColor="secondary"
        indicatorColor="secondary"
        aria-label="secondary tabs example"
      >
        <Tab label="Menu/ Service" />
        <Tab label="Posts" />
        <Tab label="Reviews" />
      </Tabs>
      <TabPanel value={value} index={0}>
        {/* content for tab 1 */}
        <ProfileForm />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Gallery itemData={data}/>
      </TabPanel>
      <TabPanel value={value} index={2}>
        {/* Content for Tab Three */}
                {cardData.map((card, index) => (
                  <div key={index} style={{ marginBottom: '15px' }}>
                    <ProfileReview
                      key={index} // Use a unique key for each card
                      title={card.title}
                      avatarImage={card.avatarImage}
                      reviews={card.reviews}
                      content={card.content}
                      onFavoriteClick={card.onFavoriteClick}
                      onCommentClick={card.onCommentClick}
                    />
                  </div>

                ))}
      </TabPanel>
    </Box>
  );
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Box
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          {children}
        </Box>
      )}
    </Box>
  );
}

import * as React from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardActions from '@mui/material/CardActions';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import FavoriteIcon from '@mui/icons-material/Favorite';
import CommentIcon from '@mui/icons-material/Comment';
import MoreVertIcon from '@mui/icons-material/MoreVert';

export default function ProfileReview({ title, avatarImage, reviews, onFavoriteClick, onCommentClick, }) {
  return (
    <Card sx={{ maxWidth: '100%' }}>
      <CardHeader
        avatar={
          <Avatar alt="Avatar" src={avatarImage} />
        }
        
        title={title}
      />
      <div className='ms-3'>
        <h5>{reviews}</h5>
      </div>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites" onClick={onFavoriteClick}>
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="comment" onClick={onCommentClick}>
          <CommentIcon /> {/* Use CommentIcon for comment button */}
        </IconButton>
      </CardActions>
    </Card>
  );
}

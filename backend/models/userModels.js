const Sequelize = require('sequelize')
const sequelize = require('../db')

const User = sequelize.define('user', {
    name: {
        type: Sequelize.STRING,
        allowNull:false,
    },
    email: {
        type: Sequelize.STRING,
        allowNull:false,
        unique:true,
    },
    phone:{
        type: Sequelize.STRING,
        allowNull:true,
    },

    password: {
        type: Sequelize.STRING,
        allowNull:false,
    }

})

// User.sync({force:false})
//     .then(()=>console.log('Users table created'))
//     .catch((error)=>console.error('Error creating Users table: ',error))
    
module.exports = User
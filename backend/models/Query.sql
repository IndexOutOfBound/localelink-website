CREATE TABLE Business (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) UNIQUE,
    descriptions TEXT,
    locations VARCHAR(255),
    status VARCHAR(50),
    contact_info VARCHAR(255),
    category VARCHAR(100)
);
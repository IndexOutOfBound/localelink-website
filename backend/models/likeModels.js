const Sequelize = require('sequelize');
const sequelize = require('../db');
const Post = require('./postModels');
const Business = require('./businessModels');

const Like = sequelize.define('like', {
  like_id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
});

// Like.belongsTo(Post, { foreignKey: 'post_id' });
Like.belongsTo(Business, { foreignKey: 'business_id' });

module.exports = Like;
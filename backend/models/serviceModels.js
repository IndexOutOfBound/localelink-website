const Sequelize = require('sequelize')
const sequelize = require('../db')
const Business = require('./businessModels')
const Service = sequelize.define('service', {
    name: {
        type: Sequelize.STRING,
        allowNull:false,
    },
    price: {
        type: Sequelize.STRING,
        allowNull:false,
    }

})
Service.belongsTo(Business, { foreignKey: 'business_id' });
// Service.sync({force:false})
//     .then(()=>console.log('Service table created'))
//     .catch((error)=>console.error('Error creating Users table: ',error))
    
module.exports = Service
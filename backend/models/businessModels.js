const Sequelize = require('sequelize')
const sequelize = require('../db')

const Business = sequelize.define('business', {
    name: {
        type: Sequelize.STRING,
        allowNull:false,
    },
    email: {
        type: Sequelize.STRING,
        allowNull:false,
        unique:true,
    },
    phone:{
        type: Sequelize.STRING,
        allowNull:true,
    },
    category:{
        type: Sequelize.STRING,
        allowNull:true,
    },
    description:{
        type: Sequelize.STRING,
        allowNull:true,
    },
    location:{
        type: Sequelize.STRING,
        allowNull:true,
    },
    status:{
        type: Sequelize.BOOLEAN,
        allowNull:true,
    },
    password: {
        type: Sequelize.STRING,
        allowNull:false,
    }

})

// Business.sync({force:true})
//     .then(()=>console.log('Business table created'))
//     .catch((error)=>console.error('Error creating Business table: ',error))
    
module.exports = Business
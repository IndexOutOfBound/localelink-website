// models/commentModel.js
const Sequelize = require('sequelize');
const sequelize = require('../db');
const User = require('./userModels');
const Post = require('./postModels');
const Business = require('./businessModels');

const Comment = sequelize.define('comment', {
  comment_id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  content: {
    type: Sequelize.STRING,
    allowNull: false
  }
});

// Comment.belongsTo(User, { foreignKey: 'user_id' });
Comment.belongsTo(Post, { foreignKey: 'post_id' });
Comment.belongsTo(Business,{foreignKey: 'business_id'});

// Comment.sync({ force: false })
//   .then(() => console.log('Comments table created'))
//   .catch((error) => console.error('Error creating Comments table: ', error));

module.exports = Comment;

// postModels.js
const Sequelize = require('sequelize');
const sequelize = require('../db');
const Business = require('./businessModels');
const Like = require('./likeModels'); // Import Like model

const Post = sequelize.define('post', {
  post_id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  image_path: {
    type: Sequelize.STRING,
    allowNull: false
  },
  image_captions: {
    type: Sequelize.STRING,
    allowNull: true
  }
});

Post.belongsTo(Business, { foreignKey: 'business_id' });
Post.hasMany(Like, { foreignKey: 'post_id' });

module.exports = Post;

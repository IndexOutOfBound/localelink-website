const {Pool} =require ('pg');
require('dotenv').config()
const Sequlize = require('sequelize')

// const pool = new Pool({
//     user: process.env.DB_USER,
//     host: process.env.DB_HOST,
//     database: process.env.DB_NAME,
//     password: process.env.DB_PASSWORD,
//     port: process.env.DB_PORT,
// })

const sequelize = new Sequlize({
    dialect: 'postgres',
    database: 'localelinkdb',
    username: 'localelink',
    password: 'root',
    host: 'localhost',
    port: '5433',
})

async function connectToDatabase() {
    try {
      await sequelize.authenticate();
      console.log('Connected to database');
    } catch (error) {
      console.error('Error connecting to database:', error);
      process.exit(1);
    }
  }
  
connectToDatabase();
module.exports = sequelize;
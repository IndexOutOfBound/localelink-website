const Post = require('../models/postModels');
const Business = require('../models/businessModels');

const createPost = async(req,res)=>{
    try {
        // Assuming you have the business_id available from the user's session or request payload
        const { business_id } = req.body;
    
        // Create a new post with image path from multer and other data from req.body
        const newPost = await Post.create({
          business_id: business_id, // Assign the business_id to the new post
          image_path: req.file.path, // Get the uploaded file path from multer
          image_captions: req.body.caption, // Assuming image_captions is sent in the request body
          // Add other fields here as needed
        });
        res.status(201).json({ message: 'Post created successfully', data: newPost });
      } catch (error) {
        console.error('Error creating post: ', error);
        res.status(500).json({ message: 'Error creating post' });
      }
}

const getPost = async(req,res)=>{
   
}

const getAllPosts = async(req,res)=>{
  try {
    const posts = await Post.findAll({
      include: {
        model: Business,
        attributes: ['name'] // Include business name if needed
      }
    });
    res.json({ data: posts });
  } catch (error) {
    console.log(error)
    res.status(500).json({ error: 'Failed to fetch posts' });
  }
}

const updatePost = async(req,res)=>{

}

const deletePost = async(req,res)=>{
  
}
const getPostByBusinessId = async (req, res) => {
  const { business_id } = req.params; // Assuming the business ID is passed as a route parameter

  try {
    // Find all posts belonging to the specified business ID
    const posts = await Post.findAll({ where: { business_id: business_id } });

    if (!posts || posts.length === 0) {
      return res.status(404).json({ message: 'No posts found for this business ID' });
    }

    // If posts are found, return them
    res.json(posts);
    console.log(posts);
  } catch (error) {
    console.error('Error fetching posts by business ID:', error);
    res.status(500).json({ message: 'Server Error' });
  }
};
module.exports = {
    createPost,
    getAllPosts,
    getPost,
    updatePost,
    deletePost,
    getPostByBusinessId
}
const express = require('express');
const pool = require('../db');
const {hashedPassword} = require('../utils')
const Business = require('../models/businessModels')
const bcrypt = require("bcrypt")


const getAllBusiness = async (req,res ) => {
  try{
    const business = await Business.findAll();
    res.status(200).json({message: 'Business created successfully',data: business});
  }catch(error){
    console.error('Error fetching businesses: ', error);
    res.status(500).json({message: 'Error reetriveing businesses: ', error});
  }
};

const createBusiness = async (req, res) => {
  try {
    console.log("req body", req.body);

    // Check if a business with the same email already exists
    const existingBusiness = await Business.findOne({ where: { email: req.body.email } });
    if (existingBusiness) {
      return res.status(400).json({ message: 'Business with this email already exists' });
    }

    // Hash the password
    const hashedPassword = await bcrypt.hash(req.body.password, 10);

    // If no existing business found with the same email, create a new one with hashed password
    const newBusiness = await Business.create({ ...req.body, password: hashedPassword });
    res.status(200).json({ message: 'Business created successfully', data: newBusiness });
  } catch (error) {
    console.error('Error creating business: ', error);
    if (error.name === 'SequelizeValidationError') {
      res.status(400).json({ message: 'Error creating business: ' + error.errors[0].message });
    } else {
      res.status(500).json({ message: 'Error creating business' });
    }
  }
};

const getBusinessById = async (req, res) => {
  const { id } = req.params; // Assuming the business ID is passed as a route parameter
  console.log(id);
  try {
    // Find the business by ID
    const business = await Business.findByPk(id);

    if (!business) {
      return res.status(404).json({ message: 'Business not found' });
    }
    console.log(business)
    // If business is found, return it
    res.json(business);
  } catch (error) {
    console.error('Error fetching business by ID:', error);
    res.status(500).json({ message: 'Server Error' });
  }
};

const updateBusinessById = async (req,res) => {

}

const deleteBusinessById = async (req,res) => {

}

const login = async (req, res) => {
  const { email, password } = req.body;
  try {
    // Check if a business with the provided email exists
    const business = await Business.findOne({ where: { email } });
    if (!business) {
      return res.status(400).json({ message: 'Invalid email or password' });
    }
    
    // Verify password
    const isValidPassword = await bcrypt.compare(password, business.password);
    if (!isValidPassword) {
      return res.status(400).json({ message: 'Invalid email or password' });
    }

    // Send business_id, name, and email to frontend
    res.status(200).json({
      business_id: business.id,
      name: business.name,
      email: business.email,
    });
  } catch (error) {
    console.error('Error logging in: ', error);
    res.status(500).json({ message: 'Error logging in' });
  }
};


module.exports = {
    getAllBusiness,
    createBusiness,
    getBusinessById,
    updateBusinessById,
    deleteBusinessById,
    login
}
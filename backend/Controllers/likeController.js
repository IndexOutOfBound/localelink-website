const Like = require('../models/likeModels')

const addlike = async (req,res)=>{
    const {user_id, post_id}= req.body;
    const business_id = user_id;
    try{
        const like = await Like.create({business_id, post_id});
        res.status(201).json({message: 'Post liked', like})
    }catch(e){
        console.log(e);
        res.status(500).json({error: 'Error liking poost'})
    }
}

const deleteLike = async (req,res)=>{
    const {user_id,post_id} = req.body;
    const business_id = user_id;
    try{
        await Like.destroy({where: {business_id, post_id}});
        res.status(200).json({message: 'Post unliked'});
    }catch(error){
        console.log(error);
        res.status(500).json({error: 'Error unliking post'});
    }
}

module.exports = {
    addlike,
    deleteLike,
}
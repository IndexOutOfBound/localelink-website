const Comment = require('../models/commentModels')

const addComment = async (req,res)=>{
    const {user_id, post_id, content} = req.body;
    const business_id = user_id;
    try{
        const comment = await Comment.create({business_id, post_id,content})
        res.status(201).json({message: 'Comment added', comment});
    }catch(error){
        res.status(500).json({error: 'Error adding comment'});
    }
}

const getComment = async(req,res)=>{
    const {post_id} = req.params;
    try{
        const comments = await Comment.findAll({where: {post_id}});
        res.status(200).json({comments});
    }catch(error){
        res.status(500).json({error: 'Error fetching comments'});
    }
}

module.exports = {
    addComment,
    getComment
}
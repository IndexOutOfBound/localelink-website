const express = require('express')
const pool = require('../db')
const multer = require('multer')
const router =express.Router();


const {
    createPost,
    getPost,
    getAllPosts,
    updatePost,
    deletePost,
    getPostByBusinessId
} = require('../Controllers/postController');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads/'); // Specify the upload directory
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '-' + file.originalname); // Generate unique filename
    }
  });
  
  // Initialize multer upload
  const upload = multer({ storage: storage });
  
  // Route to handle file upload and create post
router.post('/', upload.single('image'), createPost);

router
    .route('/')
    .get(getAllPosts)
    


router
    .route('/:id')
    .get(getPost)
    .put(updatePost)
    .delete(deletePost)

router.get('/business/:business_id', getPostByBusinessId)

module.exports = router 
const express = require('express')
const pool = require('../db')
const router =express.Router();

const {addComment, getComment} = require('../Controllers/commentController')

router
    .route('/')
    .post(addComment)

router.get('/:post_id',getComment)

module.exports= router
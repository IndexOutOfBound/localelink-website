const express = require('express')
const pool = require('../db')
const router =express.Router();

const {
    getAllBusiness, 
    createBusiness, 
    getBusinessById, 
    updateBusinessById, 
    deleteBusinessById,
    login
    } = require('../Controllers/businessController')

router
    .route('/')
    .get(getAllBusiness)
    .post(createBusiness)

router.post('/login', login)

router
    .route('/:id')
    .get(getBusinessById)
    .put(updateBusinessById)
    .delete(deleteBusinessById)


module.exports = router 
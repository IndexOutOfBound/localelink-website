const express = require('express');
const router = express.Router();
const User = require('../models/userModels')
const bcrypt = require("bcrypt")

router.post('/register', async (req, res) => {
    try {
        console.log("req body", req.body);
    
        // Check if a business with the same email already exists
        const existingUser = await User.findOne({ where: { email: req.body.email } });
        if (existingUser) {
          return res.status(400).json({ message: 'User with this email already exists' });
        }
    
        // Hash the password
        const hashedPassword = await bcrypt.hash(req.body.password, 10);
    
        // If no existing business found with the same email, create a new one with hashed password
        const newUser = await User.create({ ...req.body, password: hashedPassword });
        res.status(200).json({ message: 'User created successfully', data: newUser });
      } catch (error) {
        console.error('Error creating user: ', error);
        if (error.name === 'SequelizeValidationError') {
          res.status(400).json({ message: 'Error creating user: ' + error.errors[0].message });
        } else {
          res.status(500).json({ message: 'Error creating user' });
        }
      }
})

router.post('/login', async (req, res) => {
    const { email, password } = req.body;
  try {
    // Check if a business with the provided email exists
    const user = await User.findOne({ where: { email } });
    if (!user) {
      return res.status(400).json({ message: 'Invalid email or password' });
    }
    
    // Verify password
    const isValidPassword = await bcrypt.compare(password, user.password);
    if (!isValidPassword) {
      return res.status(400).json({ message: 'Invalid email or password' });
    }

    // Send business_id, name, and email to frontend
    res.status(200).json({
      user_id: user.id,
      name: user.name,
      email: user.email,
    });
  } catch (error) {
    console.error('Error logging in: ', error);
    res.status(500).json({ message: 'Error logging in' });
  }
})

module.exports = router;

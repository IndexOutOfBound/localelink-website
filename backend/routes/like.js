const express = require('express')
const pool = require('../db')
const router =express.Router();

const {addlike, deleteLike} = require('../Controllers/likeController')

router
    .route('/')
    .post(addlike)
    .delete(deleteLike)

module.exports= router
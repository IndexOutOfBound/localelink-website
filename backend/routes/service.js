const express = require('express');
const router = express.Router();
const Service = require('../models/serviceModels');

// Create a new service
router.post('/', async (req, res) => {
  try {
    const { service, price, business_id } = req.body;
    const name = service;
    const result = await Service.create({ name, price, business_id });
    res.status(201).json(result);
  } catch (error) {
    console.error('Error creating service:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Delete a service by ID
router.delete('/:id', async (req, res) => {
  const id = req.params.id;
  try {
    const service = await Service.findByPk(id);
    if (!service) {
      return res.status(404).json({ error: 'Service not found' });
    }
    await service.destroy();
    res.status(204).end();
  } catch (error) {
    console.error('Error deleting service:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Get all services
router.get('/', async (req, res) => {
  try {
    const services = await Service.findAll();
    res.status(200).json(services);
  } catch (error) {
    console.error('Error fetching services:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Get service by ID
router.get('/:id', async (req, res) => {
  const id = req.params.id;
  try {
    const service = await Service.findByPk(id);
    if (!service) {
      return res.status(404).json({ error: 'Service not found' });
    }
    res.status(200).json(service);
  } catch (error) {
    console.error('Error fetching service:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

router.get('/business/:business_id', async (req, res) => {
    const business_id = req.params.business_id;
    try {
      const services = await Service.findAll({ where: { business_id } });
      if (services.length === 0) {
        return res.status(404).json({ error: 'No services found for the provided business ID' });
      }
      res.status(200).json(services);
    } catch (error) {
      console.error('Error fetching services by business ID:', error);
      res.status(500).json({ error: 'Internal server error' });
    }
  });

  router.get('/business', async (req, res) => {
    const {business_id} = req.query;
    try {
      const services = await Service.findAll({ where: { business_id } });
      if (services.length === 0) {
        return res.status(404).json({ error: 'No services found for the provided business ID' });
      }
      res.status(200).json(services);
    } catch (error) {
      console.error('Error fetching services by business ID:', error);
      res.status(500).json({ error: 'Internal server error' });
    }
  });

module.exports = router;

const express = require('express');
const dotenv = require('dotenv');
const cors = require('cors');

const businessRoutes = require('./routes/business')
const postRoutes = require('./routes/post')
const likeRoutes = require('./routes/like')
const commentRoutes = require('./routes/comment')
const serviceRoutes = require('./routes/service')
const userRoutes = require('./routes/user')

dotenv.config()

const port =process.env.DB_PORT || 3002
const app = express();
app.use(express.json());
app.use(cors())
app.use('/uploads', express.static('uploads'));
// Routes
app.use('/business', businessRoutes)
app.use('/posts', postRoutes)
app.use('/likes', likeRoutes);
app.use('/comments', commentRoutes);
app.use('/service', serviceRoutes);
app.use('/user', userRoutes);

app.listen(port, ()=> {
    console.log('listening on port ' + port)
})